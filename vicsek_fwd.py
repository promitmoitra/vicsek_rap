import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import colors
import time

def check_update_move(locations,states,thetas):
    global L; global mu; global r; global phi
    global tau_i; global tau_r; global tau_0
    num_points = len(locations)
    next_states = np.zeros(num_points)
    next_thetas = np.zeros(num_points)
##    increments = np.zeros((locations.shape))
    increments = mu*np.array([np.cos(thetas.T),np.sin(thetas.T)]).reshape(2,num_points).T
    next_locations = locations + increments

    for i in range(num_points):
        pair_diff = locations-locations[i]
        pair_dist_sq = np.sum(np.square(pair_diff),axis=1)
        rel_bear = np.arctan2(pair_diff[:,1],pair_diff[:,0])-thetas[i]
        rel_bear[i] = 0 
        rel_bear = (rel_bear+np.pi)%(2*np.pi)-np.pi
##        nbr_idx = np.logical_and(np.logical_and(pair_dist_sq>0,pair_dist_sq<np.square(r)),rel_bear<phi)
##        nn = np.arange(num_points)[nbr_idx]
##        nn = np.logical_and(np.logical_and(pair_dist_sq>0,pair_dist_sq<np.square(r)),np.absolute(rel_bear)<phi)
        nn = np.logical_and(pair_dist_sq<np.square(r),np.absolute(rel_bear)<phi)
##        nn_s = states[nn]==0
##        nn_i = np.logical_and(states[nn]>0,states[nn]<=tau_i)
##        nn_r = np.logical_and(states[nn]>tau_i,states[nn]<tau_0)

##        arg_s_nn = thetas[i]+0.5*np.random.uniform(-np.pi,np.pi) if np.sum(nn_s)==0 else np.mean(thetas[nn[nn_s]])
##        arg_s_nn = np.mean(thetas[nn]) if np.sum(nn_s)==0 else np.mean(thetas[nn[nn_s]])
##        arg_i_nn = np.mean(thetas[nn]) if np.sum(nn_i)==0 else np.mean(thetas[nn[nn_i]])
##        arg_r_nn = np.mean(thetas[nn]) if np.sum(nn_r)==0 else np.mean(thetas[nn[nn_r]])
##        arg_s_nn = arg_i_nn if np.sum(nn_s)==0 else np.mean(thetas[nn[nn_s]])
##        step=mu

        if states[i]==0:
            nn_i = np.logical_and(states[nn]>0,states[nn]<=tau_i)

##            step = np.random.normal(0,mu)
            next_thetas[i] = thetas[i]+np.random.uniform(-np.pi,np.pi)

##            l_switch = np.random.random(); p_spon = 0.1
##            if l_switch<np.sum(nn_i)/len(nn):#or l_switch<p_spon:
            if np.sum(nn_i)>0:
                next_states[i] = 1

        if states[i]>0 and states[i]<=tau_i:
            next_thetas[i] = np.mean(thetas[nn])+0.3*(np.random.uniform(-np.pi,np.pi))
##            next_thetas[i] = np.mean(thetas[nn]*(r-pair_dist_sq[nn])/r)+0.15*(np.random.uniform(-np.pi,np.pi))
            next_states[i] = (states[i]+1)%tau_0

##        if states[i]>tau_i:
##            next_thetas[i] = np.mean(thetas[nn])+0.15*np.random.uniform(-np.pi,np.pi)            
##            next_thetas[i] = thetas[i]
##            next_states[i] = (states[i]+1)%tau_0

##        increments[i] = np.array([step*np.cos(thetas[i]),step*np.sin(thetas[i])])        

##    next_locations = (next_locations)%L
    return next_locations,next_states.T,next_thetas.T

def mainloop(N,step,r):
    global L; global init_bound; global i0; global r0
    global tau_i; global tau_r; global tau_0
    init_loc = np.random.uniform(L/2-init_bound,L/2+init_bound,size=(N,2)) 
##    init_thetas = np.random.uniform(0,2*np.pi,size=(N,1))
    init_thetas = np.linspace(0,2*np.pi,N).reshape(N,1);np.random.shuffle(init_thetas)
    init_states=np.ones(N)
##    init_states[0:int(i0*N)] = np.random.randint(1,tau_i+1,size=int(i0*N))
##    init_states[int(i0*N):int(i0*N)+int(r0*(1-i0)*N)] = np.random.randint(tau_i+1,tau_0,size=int(r0*(1-i0)*N))
##    init_states[int(i0*N):int(i0*N)+int(r0*N)] = np.random.randint(tau_i+1,tau_0,size=int(r0*N))
    np.random.shuffle(init_states)

    walkers = np.concatenate((init_loc,init_states.reshape(N,1),init_thetas),axis=1)
    rem = 0
    ts_data = np.zeros((T,3))
    wlk_data = np.zeros((T,N,4))
    for t in range(T):
        rem = t
        wlk_data[t] = walkers.copy()
        locations = walkers[:,:2]
        states = walkers[:,2]#;states[23]=1
        thetas = walkers[:,3]
        thetas = (thetas)%(2*np.pi)
        
        scount = np.sum(states==0)
##        icount = np.sum(np.logical_and(states>0,states<=tau_i))
##        rcount = np.sum(np.logical_and(states>tau_i,states<tau_0))
##        ts_data[t] = np.array([scount,icount,rcount])
        if scount==N:
            break

        walkers[:,:2],walkers[:,2],walkers[:,3] = check_update_move(locations,states,thetas)

    return wlk_data,ts_data,rem

def plot(walkers,ax=None):
    global L; global tau_0    
    if ax==None:
        fig,ax = plt.subplots()
    ax.clear()
    ax.set_xlim([0,L])
    ax.set_xticks(np.arange(0,L,100))
    ax.set_ylim([0,L])
    ax.set_yticks(np.arange(0,L,100))
    ax.set_aspect('equal')
    plt.title("$t\\ =\\ {}$".format(0))
    img = ax.scatter(walkers[0],walkers[1],marker='o',s=7,c=walkers[2],
                    edgecolor='k',linewidth=0.12,
                    cmap=lcmap,vmin=0,vmax=tau_0)
    return img,

def animate(wlk_data,ts_data,ax=None,ini=0,fin=1):
    global tau_i; global tau_r; global n; global r
    global path
    if ax==None:
        fig,ax = plt.subplots(figsize=(5,5))
    img, = plot(wlk_data[0,:,:3].T,ax=ax)
##    scount,icount,rcount = ts_data[0]
##    txt = ax.text(L+0.01,0.,"$S_t:\\ {0:d}$\n\n$I_t:\\ {1:d}$\n\n$R_t:\\ {2:d}$".format(int(scount),int(icount),int(rcount)))
    def update(n,wlk_data,ax):
        if n<=len(wlk_data):
            img.set_offsets(wlk_data[n,:,:2])
            img.set_array(wlk_data[n,:,2].T)            
            plt.title("$t\\ =\\ {}$".format(n))
##            scount,icount,rcount = ts_data[n]
##            txt.set_text("$S_t:\\ {0:d}$\n\n$I_t:\\ {1:d}$\n\n$R_t:\\ {2:d}$".format(int(scount),int(icount),int(rcount)))
        else:
            ani.event_source.stop()
        return img,#txt,
    ani = animation.FuncAnimation(fig,update,range(ini,fin),fargs=(wlk_data,ax),
                                  interval=50,repeat=False)
##    ani.save('rap_vic.gif')
    plt.show()
    return


if __name__ == '__main__':
    tau_i = 2000
    tau_r = 0
    tau_0 = tau_i+tau_r+1
    lcmap = colors.ListedColormap(['xkcd:pale grey']+['xkcd:darkish red']*tau_i+['xkcd:almost black']*tau_r)
 
    T = tau_i; L = 100; init_bound = 1
    n = 100
    mu = 0.01#5*L/1000#0.05
    r = 1.0#2*L/100#0.2
    i0 = 0.1; r0 = 0.
    phi = np.pi/9

    wlk,ts,rem = mainloop(n,mu,r)
    path = 'data/'
    start = time.monotonic()
    wlk,ts,rem = mainloop(n,mu,r)
    print(rem)
    end = time.monotonic()
##    np.save(path+'wlk_data_{0:02d}-{1:02d}_n-{2:03d}_r-{3:.3f}_mu-{4:.3f}_actpas.npy'.format(tau_i,tau_r,n,r,mu).replace('.','-',2),wlk[:rem]) 
##    np.save(path+'ts_data_{0:02d}-{1:02d}_n-{2:03d}_r-{3:.3f}_mu-{4:.3f}_actpas.npy'.format(tau_i,tau_r,n,r,mu).replace('.','-',2),ts[:rem])
##    wlk = np.load(path+'wlk_data_{0:02d}-{1:02d}.npy'.format(tau_i,tau_r)) 
##    ts = np.load(path+'ts_data_{0:02d}-{1:02d}.npy'.format(tau_i,tau_r))
    animate(wlk,ts,ini=0,fin=min(rem,T))
    print('\nRuntime: ',time.strftime("%H:%M:%S",time.gmtime(end-start)),'\n')
