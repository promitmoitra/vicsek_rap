#!/bin/bash
#PBS -N vicsek_numba 
#PBS -q debugq
#PBS -l select=1:ncpus=40
#PBS -l walltime=00:30:00
#PBS -o ./pbsOUT/
#PBS -j oe
#PBS -V

module load anaconda/3
cd $PBS_O_WORKDIR

source activate wlk
python3 vicsek_numba.py
