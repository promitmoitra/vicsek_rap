import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import colors
##from scipy.spatial import cKDTree
from numba_kdtree import KDTree
from numba import i8,jit,njit,prange
import time


@njit(parallel=True)
def check_update_move(tree,locations,thetas,states):
    global L; global mu; global r;
    global tau_i; global tau_r; global tau_0
    num_points = len(locations)
    next_states = np.zeros(num_points)
    next_thetas = np.zeros(num_points)
    increments = np.zeros((locations.shape))

    for i in prange(num_points):
##        nn = np.array(tree.query_ball_point(locations[i],r=r),dtype=np.int)
        nn = tree.query_radius(locations[i],r=r)[0]
        nn_s = states[nn]==0
##        nn_i = states[nn]==1
##        nn_r = states[nn]==2
        nn_i = np.logical_and(states[nn]>0,states[nn]<=tau_i)
        nn_r = np.logical_and(states[nn]>tau_i,states[nn]<tau_0)

##        nn_sir = np.array([np.sum(nn_s),np.sum(nn_i),np.sum(nn_r)])
##        maj_nn = np.max(nn_sir)
##        next_states[i] = np.where(nn_sir==maj_nn)[0][0]

        arg_s_nn = np.mean(thetas[nn]) if np.sum(nn_s)==0 else np.mean(thetas[nn[nn_s]])
        arg_i_nn = np.mean(thetas[nn]) if np.sum(nn_i)==0 else np.mean(thetas[nn[nn_i]])
        arg_r_nn = np.mean(thetas[nn]) if np.sum(nn_r)==0 else np.mean(thetas[nn[nn_r]])

        step=mu

        if states[i]==0:
            p_spon = 0.1
##            nn = np.array(tree.query_ball_point(locations[i],r=1.2*r),dtype=np.int)
##            nn = np.array(tree.query_radius(locations[i],r=1.2*r),dtype=np.int)
##            nn_i = np.logical_and(states[nn]>0,states[nn]<=tau_i)

##            step=mu
            next_thetas[i] = np.random.uniform(0,2*np.pi)
##            next_thetas[i] = arg_s_nn+0.5*(np.random.uniform(-np.pi,np.pi))            
            l_switch = np.random.random()
            if l_switch<np.sum(nn_i)/len(nn):#or l_switch<p_spon:
                next_states[i] = 1

        if states[i]>0 and states[i]<=tau_i:
##            nn = np.array(tree.query_ball_point(locations[i],r=1.*r),dtype=np.int) 
##            nn = np.array(tree.query_radius(locations[i],r=1.2*r),dtype=np.int)
##            nn_s = states[nn]==0
##            arg_s_nn = np.mean(thetas[nn]) if np.sum(nn_s)==0 else np.mean(thetas[nn[nn_s]])

##            step=mu
##            next_thetas[i] += 0+0.*np.random.uniform(-np.pi,np.pi)
##            next_thetas[i] = np.mean(thetas[nn])+0.15*(np.random.uniform(-np.pi,np.pi))
##            next_thetas[i] = np.mean([arg_s_nn,arg_r_nn])+0.15*(np.random.uniform(-np.pi,np.pi))
            next_thetas[i] = arg_s_nn+0.15*(np.random.uniform(-np.pi,np.pi))
            next_states[i] = (states[i]+1)%tau_0

        if states[i]>tau_i:
##            nn = np.array(tree.query_ball_point(locations[i],r=0.5*r),dtype=np.int)
##            nn = np.array(tree.query_radius(locations[i],r=1.2*r),dtype=np.int)
##            nn_s = states[nn]==0
##            nn_i = np.logical_and(states[nn]>0,states[nn]<=tau_i)
##            arg_s_nn = np.mean(thetas[nn]) if np.sum(nn_s)==0 else np.mean(thetas[nn[nn_s]])
##            arg_i_nn = np.mean(thetas[nn]) if np.sum(nn_i)==0 else np.mean(thetas[nn[nn_i]])

##            step=mu
##            next_thetas[i] += 0+0.*np.random.uniform(-np.pi,np.pi)            
            next_thetas[i] = np.mean(thetas[nn])+0.15*np.random.uniform(-np.pi,np.pi)            
##            next_thetas[i] = np.mean([arg_i_nn,arg_r_nn])+0.15*(np.random.uniform(-np.pi,np.pi))
##            next_thetas[i] = arg_s_nn+0.15*(np.random.uniform(-np.pi,np.pi))
            next_states[i] = (states[i]+1)%tau_0

        increments[i] = np.array([step*np.cos(thetas[i]),step*np.sin(thetas[i])])        

    next_locations = locations + increments
    next_locations = (next_locations)%L
    return next_locations.T,next_states,next_thetas

##@jit
def mainloop(N,step,r):
    global L; global init_bound; global i0; global r0
    global tau_i; global tau_r; global tau_0
    init_loc = np.random.uniform(L/2-init_bound,L/2+init_bound,size=(2,N)) 
    sinit = np.zeros(N)
    sinit[0:int(i0*N)] = np.random.randint(1,tau_i+1,size=int(i0*N))
##    sinit[int(i0*N):int(i0*N)+int(r0*(1-i0)*N)] = np.random.randint(tau_i,tau_0,size=int(r0*(1-i0)*N))
    sinit[int(i0*N):int(i0*N)+int(r0*N)] = np.random.randint(tau_i,tau_0,size=int(r0*N))
    np.random.shuffle(sinit)

    walkers = np.concatenate((init_loc,sinit.reshape(1,N)),axis=0)
    rem = 0
    ts_data = np.zeros((T,3))
    wlk_data = np.zeros((T,3,N))
    thetas = np.random.uniform(0,2*np.pi,size=N)
    for t in range(T):
        rem = t
        wlk_data[t] = walkers.copy()
        locations = walkers[:-1].T
        states = np.copy(walkers[-1])

        scount = np.sum(states==0)
        icount = np.sum(np.logical_and(states>0,states<=tau_i))
        rcount = np.sum(np.logical_and(states>tau_i,states<tau_0))
        ts_data[t] = np.array([scount,icount,rcount])
        if scount==N:
            break

        tree = KDTree(locations)
##        tree = cKDTree(locations,boxsize=[L,L])

        walkers[:-1],walkers[-1],thetas = check_update_move(tree,locations,thetas,states)

    return wlk_data,ts_data,rem

def plot(walkers,ax=None):
    if ax==None:
        fig,ax = plt.subplots()
    ax.clear()
    ax.set_xlim([0,L])
    ax.set_ylim([0,L])
    ax.set_aspect('equal')
    plt.title("$t={}$".format(0))
    img = ax.scatter(walkers[0],walkers[1],marker='o',s=7,c=walkers[2],
                    edgecolor='k',linewidth=0.12,
                    cmap=lcmap,vmin=0,vmax=tau_0)
    return img,

def animate(wlk_data,ts_data,rem,ax=None,ini=0):
    if ax==None:
        fig,ax = plt.subplots(figsize=(10,10))
    img, = plot(wlk_data[0],ax=ax)
    scount,icount,rcount = ts_data[0]
    txt = ax.text(L+0.01,0.,"$S_t:\\ {0:d}$\n\n$I_t:\\ {1:d}$\n\n$R_t:\\ {2:d}$".format(int(scount),int(icount),int(rcount)))
    def update(n,wlk_data,ax):
        if n<=rem:
            img.set_offsets(wlk_data[n,:-1].T)
            img.set_array(wlk_data[n,-1])            
            plt.title("$t={}$".format(n))
            scount,icount,rcount = ts_data[n]
            txt.set_text("$S_t:\\ {0:d}$\n\n$I_t:\\ {1:d}$\n\n$R_t:\\ {2:d}$".format(int(scount),int(icount),int(rcount)))
        else:
            ani.event_source.stop()
        return img,txt,
    ani = animation.FuncAnimation(fig,update,range(ini,rem),fargs=(wlk_data,ax),
                                  interval=50,repeat=False)
    ani.save('rap_vic.gif')
##    plt.show()
    return


if __name__ == '__main__':
    tau_i = 25
    tau_r = 5
    tau_0 = tau_i+tau_r+1
    lcmap = colors.ListedColormap(['xkcd:pale grey']+['xkcd:darkish red']*tau_i+['xkcd:almost black']*tau_r)
 
    T = 5000; L = 1; init_bound = 0.05#L/2
    n = 50
    mu = 0.01
    r = 0.05
    i0 = 0.1; r0 = 0.1

    start = time.monotonic()
##    wlk,ts,rem = mainloop(n,mu,r)
##    print(rem)
##    np.save('./wlk_data.npy',wlk)
    end = time.monotonic()
    print('\nRuntime: ',time.strftime("%H:%M:%S",time.gmtime(end-start)),'\n')

    wlk = np.load('./wlk_data.npy')
    animate(wlk,np.zeros((T,3)),T)#,ini=500)
##    trans=500
##    mu = np.mean(ts[trans:,0]/n)
##    sig = np.std(ts[trans:,0]/n)
##    plt.plot(range(T),ts[:,0]/n)
##    plt.plot(range(trans,T),np.ones(T-trans)*(mu+3*sig))
##    plt.show()
