import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import colors
import time

def check_update_move(locations,states,thetas):
    global L; global mu; global r; global phi
    global tau_i; global tau_r; global tau_0
    num_points = len(locations)
    next_states = np.zeros(num_points)
    next_thetas = thetas
    increments = mu*np.array([np.cos(thetas.T),np.sin(thetas.T)]).reshape(2,num_points).T
    next_locations = locations + increments

    for i in range(num_points):
        pair_diff = locations-locations[i]
        pair_dist_sq = np.sum(np.square(pair_diff),axis=1)
        rel_bear = np.arctan2(pair_diff[:,1],pair_diff[:,0])-thetas[i]
        rel_bear[i] = 0 
        rel_bear = (rel_bear+np.pi)%(2*np.pi)-np.pi

        nn = np.logical_and(pair_dist_sq<np.square(r),np.absolute(rel_bear)<phi)
        print(rel_bear/np.pi,nn)
    return next_locations,next_states.T,next_thetas.T

def mainloop(N,step,r):
    global L; global init_bound; global i0; global r0
    global tau_i; global tau_r; global tau_0

    init_thetas = np.array([9*np.pi/4]+list(np.linspace(-np.pi,3*np.pi/4,N-1))).reshape(N,1)
    init_loc = np.concatenate((np.cos(init_thetas),np.sin(init_thetas)),axis=1); init_loc[0] = np.array([0,0])
    init_states = np.zeros(N).reshape(N,1);init_states[0] = 1

    walkers = np.concatenate((init_loc,init_states.reshape(N,1),init_thetas),axis=1)
    rem = 0
    ts_data = np.zeros((T,3))
    wlk_data = np.zeros((T,N,4))
    for t in range(T):
        rem = t
        wlk_data[t] = walkers.copy()
        locations = walkers[:,:2]
        states = walkers[:,2]
        thetas = walkers[:,3]
        thetas = (thetas+np.pi)%(2*np.pi)-np.pi
        walkers[:,:2],walkers[:,2],walkers[:,3] = check_update_move(locations,states,thetas)

    return wlk_data,ts_data,rem

if __name__ == '__main__':
    tau_i = 10
    tau_r = 0
    tau_0 = tau_i+tau_r+1
    lcmap = colors.ListedColormap(['xkcd:pale grey']+['xkcd:darkish red']*tau_i+['xkcd:almost black']*tau_r)
 
    T = 2; L = 1; init_bound = 1
    n = 9
    mu = 0.
    r = 10.0
    i0 = 0.1; r0 = 0.
    phi = 3*np.pi/4

    wlk,ts,rem = mainloop(n,mu,r)
    tstep=1
    plt.scatter(wlk[0,:,0],wlk[0,:,1])
    plt.quiver(wlk[tstep,:,0],wlk[tstep,:,1],np.cos(wlk[tstep,:,-1]),np.sin(wlk[tstep,:,-1]))
    plt.show()
