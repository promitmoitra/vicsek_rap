import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import colors
from scipy.spatial import cKDTree
##from numba_kdtree import KDTree
##from numba import njit

def check_update_move(tree,locations,thetas,step,rad):
    global L;global r
    num_points = len(locations)
    for i in range(num_points):
        nn = tree.query_ball_point(locations[i],r=rad)
        thetas[i] = np.mean(thetas[nn])+0.15*(np.random.normal(0,1))
    increments = np.concatenate((step*np.cos(thetas.reshape(num_points,1)),step*np.sin(thetas.reshape(num_points,1))),axis=1)
    new_locations = locations + increments
##    mask = np.logical_or(new_locations<0,new_locations>L)
####|Inward Scattering boundaries| 
##    new_locations[mask] = locations[mask] - increments[mask]
####|Periodic boundaries| 
    new_locations = (new_locations)%L
    return new_locations.T

##@njit
##def move(locations,step):
##    global L
##    num_points = len(locations)
##    locations = locations.T.reshape(2*num_points)
##    increments = np.random.normal(0.0,step,size=locations.shape)
##    new_locations = locations + increments
##    mask = np.logical_or(new_locations<0,new_locations>L)
######|Inward Scattering boundaries| 
####    new_locations[mask] = locations[mask] - increments[mask]
######|Periodic boundaries| 
##    new_locations[mask] = (locations[mask] + increments[mask])%L
##    new_locations = new_locations.reshape(2,num_points)
##    return new_locations

####periodic boundaries
##def check_update(tree,locations,states,rad):
##    global tau_i; global tau_0
##    num_points = len(states)
##    next_states = np.zeros(num_points)
##    for i in range(num_points):
##        if states[i] == 0:
##            nn = tree.query_ball_point(locations[i],r=rad)
##            if np.sum(np.logical_and(states[nn]>0,states[nn]<=tau_i))>0:
##                next_states[i] = 1
##        else:
##            next_states[i] = (states[i]+1)%tau_0
##    return next_states 

####numba_kdtree
##@njit
##def check_update(tree,locations,states,rad,flag):
##    global tau_i; global tau_0
##    num_points = len(states)
##    next_states = np.zeros(num_points)
##    for i in range(num_points):
##        if states[i] == 0:
##            nn = tree.query_radius(locations[i],r=rad)[0]
##            if np.sum(np.logical_and(states[nn]>0,states[nn]<=tau_i))>0:
##                next_states[i] = 1
##                flag[i] = 1
##        else:
##            next_states[i] = (states[i]+1)%tau_0
##    return next_states,flag 

####brute force
##@njit
##def check_update(locations,states):
##    global tau_i; global tau_0
##    num_points = len(states)
##    next_states = np.zeros(num_points)
##    for i in range(num_points):
##        if states[i] == 0:
##            nn = np.sqrt(np.sum((locations-locations[i])**2,axis=1))<r
##            if np.sum(np.logical_and(states[nn]>0,states[nn]<=tau_i))>0:
##                next_states[i] = 1
##        else:
##            next_states[i] = (states[i]+1)%tau_0
##    return next_states 


##@njit
def mainloop(N,step,r):
    global L; global init_bound; global frac
    global tau_i; global tau_r; global tau_0
    init_loc = np.random.uniform(L/2-init_bound,L/2+init_bound,size=(2,N)) 
    sinit=2*np.ones(N)
##    sinit[0:int(frac*N)] = np.random.randint(1,tau_i+1,size=int(frac*N))
##    np.random.shuffle(sinit)

    walkers = np.concatenate((init_loc,sinit.reshape(1,N)),axis=0)
    rem = 0
    ts_data = np.zeros((T,3))
    wlk_data = np.zeros((T,3,N))
    thetas = np.random.uniform(0,2*np.pi,size=N)
    
##    first_pass = np.zeros(N)
    for t in range(T):
        rem = t
        wlk_data[t] = walkers.copy()
        locations = walkers[:-1].T
        
##        states = np.copy(walkers[-1])

##        scount = np.sum(states==0)
##        icount = np.sum(np.logical_and(states>0,states<=tau_i))
##        rcount = np.sum(np.logical_and(states>tau_i,states<tau_0))
##        ts_data[t] = np.array([scount,icount,rcount])
##        if scount==N:
##            break

##        tree = KDTree(locations)
        tree = cKDTree(locations,boxsize=[L,L])

        walkers[:-1] = check_update_move(tree,locations,thetas,step,r)

##        walkers[-1] = check_update(tree,locations,states,r)
####        walkers[-1],first_pass = check_update(tree,locations,states,r,first_pass)
####        walkers[-1] = check_update(locations,states)

##        walkers[:-1] = move(locations,step)
####        if np.sum(first_pass)==N:
####            print('First pass:{0:d}'.format(t))
####            break
####        if scount==N:
####            print('Removal:{0:d}'.format(t))
####            break

    return wlk_data,ts_data,rem

def plot(walkers,ax=None):
    if ax==None:
        fig,ax = plt.subplots()
    ax.clear()
    ax.set_xlim([0,L])
    ax.set_ylim([0,L])
    ax.set_aspect('equal')
    plt.title("$t={}$".format(0))
    img = ax.scatter(walkers[0],walkers[1],s=5,c=walkers[2],
                    edgecolor='k',linewidth=0.12,
                    cmap=lcmap,vmin=0,vmax=tau_0)
    return img,

def animate(wlk_data,ts_data,rem,ax=None):
    if ax==None:
        fig,ax = plt.subplots()
    img, = plot(wlk_data[0],ax=ax)
    scount,icount,rcount = ts_data[0]
    txt = ax.text(L+0.01,0.,"$S_t:\\ {0:d}$\n\n$I_t:\\ {1:d}$\n\n$R_t:\\ {2:d}$".format(int(scount),int(icount),int(rcount)))
    def update(n,wlk_data,ax):
        if n<=rem:
            img.set_offsets(wlk_data[n,:-1].T)
            img.set_array(wlk_data[n,-1])            
            plt.title("$t={}$".format(n))
            scount,icount,rcount = ts_data[n]
            txt.set_text("$S_t:\\ {0:d}$\n\n$I_t:\\ {1:d}$\n\n$R_t:\\ {2:d}$".format(int(scount),int(icount),int(rcount)))
        else:
            ani.event_source.stop()
        return img,txt,
    ani = animation.FuncAnimation(fig,update,T,fargs=(wlk_data,ax),
                                  interval=100,repeat=False)
##    ani.save('rorschach.gif',dpi=100)
    plt.show()
    return

if __name__ == '__main__':
    tau_i = 1
    tau_r = 1 
    tau_0 = tau_i+tau_r+1
    lcmap = colors.ListedColormap(['xkcd:pale grey']+['xkcd:darkish red']*tau_i+['xkcd:almost black']*tau_r)
 
    T = 1000; L = 1; init_bound = L/2
    n = 100; 
    mu = 0.0125#0.0*L/4; 
    r = 0.05#0.05*L/2
    frac = 0.1#1/n

    wlk,ts,rem = mainloop(n,mu,r)
    animate(wlk,ts,rem)
##    np.save('wlk_r-{0:.2f}.npy'.format(r),wlk);np.save('ts.npy',ts)
##    plt.plot(range(T),ts[:,0]/n); plt.plot(range(T),ts[:,1]/n); plt.plot(range(T),ts[:,2]/n); plt.show()
##    print('Density:{0:.0f}'.format(n/(L**2)))
##    print('mu:{0:.5f}'.format(mu),end='\t')
##    print('r:{0:.5f}'.format(r))
##    print('mu/r:{0:.5f}'.format(mu/r))
##    print('First pass:{0:d}'.format(rem))
##    print('actual:{0:.5f}'.format(tau_i/tau_r))
##    print('<S>:{0:.4f}\t<I>:{1:.5f}'.format(np.mean(ts[:rem,0]/n),np.mean(ts[:rem,1]/n)),end='\t')
##    print('<R>:{0:.5f}'.format(np.mean(ts[:rem,2]/n)))
##    print('<I>/<R>:{0:.5f}'.format(np.mean(ts[:rem,1])/np.mean(ts[:rem,2])))
