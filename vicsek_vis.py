import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import colors
import time

def plot(walkers,ax=None):
    global L; global tau_0
    if ax==None:
        fig,ax = plt.subplots()
    ax.clear()
    ax.set_xlim([0,L])
    ax.set_xticks([0,L])
    ax.set_ylim([0,L])
    ax.set_yticks([0,L])
    ax.set_aspect('equal')
    plt.title("$t\\ =\\ {}$".format(0))
    img = ax.scatter(walkers[0],walkers[1],marker='o',s=7,c=walkers[2],
                    edgecolor='k',linewidth=0.14,
                    cmap=lcmap,vmin=0,vmax=tau_0)
    return img,

def animate(wlk_data,ts_data,ax=None,ini=0):
    global tau_i; global tau_r; global n; global r
    global path
    if ax==None:
        fig,ax = plt.subplots(figsize=(6,6))
    img, = plot(wlk_data[0],ax=ax)
    scount,icount,rcount = ts_data[0]
    txt = ax.text(L+0.01,0.,"$S_t:\\ {0:d}$\n\n$I_t:\\ {1:d}$\n\n$R_t:\\ {2:d}$".format(int(scount),int(icount),int(rcount)),size=8)
    def update(n,wlk_data,ax):
        if n<=len(wlk_data):
            img.set_offsets(wlk_data[n,:-1].T)
            img.set_array(wlk_data[n,-1])            
            plt.title("$t\\ =\\ {}$".format(n))
            scount,icount,rcount = ts_data[n]
            txt.set_text("$R_t:\\ {0:d}$\n\n$A_t:\\ {1:d}$\n\n$P_t:\\ {2:d}$".format(int(scount),int(icount),int(rcount)))
        else:
            ani.event_source.stop()
        return img,txt,
    ani = animation.FuncAnimation(fig,update,range(ini,len(wlk_data)),fargs=(wlk_data,ax),
                                  interval=50,repeat=False)
    ani.save(path+'rap_vic_{0:02d}-{1:02d}_n-{2:03d}_r-{3:.3f}_mu-{4:.3f}_actpas.gif'.format(tau_i,tau_r,n,r,mu).replace('.','-',2))
##    plt.show()
    return


if __name__ == '__main__':
    tau_i = 15
    tau_r = 5
    tau_0 = tau_i+tau_r+1
    lcmap = colors.ListedColormap(['xkcd:pale grey']+['xkcd:darkish red']*tau_i+['xkcd:almost black']*tau_r)

    T = 5000; L = 10
    n = 50
    mu = 5*L/1000
    r = L/100#0.1
##    i0 = 0.1; r0 = 0.1

    path = 'data/'
    start = time.monotonic()
##    wlk,ts,rem = mainloop()
##    print(rem)
##    np.save(path+'wlk_data_{0:02d}-{1:02d}.npy'.format(tau_i,tau_r),wlk[:rem]) 
##    np.save(path+'ts_data_{0:02d}-{1:02d}.npy'.format(tau_i,tau_r),ts[:rem])
    wlk = np.load(path+'wlk_data_{0:02d}-{1:02d}_n-{2:03d}_r-{3:.3f}_mu-{4:.3f}_actpas.npy'.format(tau_i,tau_r,n,r,mu).replace('.','-',2))
    ts = np.load(path+'ts_data_{0:02d}-{1:02d}_n-{2:03d}_r-{3:.3f}_mu-{4:.3f}_actpas.npy'.format(tau_i,tau_r,n,r,mu).replace('.','-',2))
    animate(wlk,ts)#,ini=500)
    end = time.monotonic()
    print('\nRuntime: ',time.strftime("%H:%M:%S",time.gmtime(end-start)),'\n')

##    trans=500
##    mu = np.mean(ts[trans:,0]/n)
##    sig = np.std(ts[trans:,0]/n)
##    plt.plot(range(T),ts[:,0]/n)
##    plt.plot(range(trans,T),np.ones(T-trans)*(mu+3*sig))
##    plt.show()
